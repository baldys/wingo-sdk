# UserAPI

All URIs are relative to *https://wingo-interview-server.herokuapp.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authenticate**](UserAPI.md#authenticate) | **POST** /auth | Log in
[**createUser**](UserAPI.md#createuser) | **POST** /user | Create New User
[**getDashboard**](UserAPI.md#getdashboard) | **GET** /user | Get Dashboard


# **authenticate**
```swift
    open class func authenticate(credential: Credential? = nil, completion: @escaping (_ data: String?, _ error: Error?) -> Void)
```

Log in

Authenticate user

### Example 
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import Wingocard

let credential = Credential(email: "email_example", password: "password_example") // Credential |  (optional)

// Log in
UserAPI.authenticate(credential: credential) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credential** | [**Credential**](Credential.md) |  | [optional] 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createUser**
```swift
    open class func createUser(credential: Credential? = nil, completion: @escaping (_ data: User?, _ error: Error?) -> Void)
```

Create New User

Create a new user.

### Example 
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import Wingocard

let credential = Credential(email: "email_example", password: "password_example") // Credential | Post the necessary fields for the API to create a new user. (optional)

// Create New User
UserAPI.createUser(credential: credential) { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credential** | [**Credential**](Credential.md) | Post the necessary fields for the API to create a new user. | [optional] 

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getDashboard**
```swift
    open class func getDashboard(completion: @escaping (_ data: String?, _ error: Error?) -> Void)
```

Get Dashboard

Get the user dashboard

### Example 
```swift
// The following code samples are still beta. For any issue, please report via http://github.com/OpenAPITools/openapi-generator/issues/new
import Wingocard


// Get Dashboard
UserAPI.getDashboard() { (response, error) in
    guard error == nil else {
        print(error)
        return
    }

    if (response) {
        dump(response)
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**String**

### Authorization

[token](../README.md#token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

